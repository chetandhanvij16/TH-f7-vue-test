import axios from 'axios';

export default {
    base: 'http://xinnature.asia/menu/wp-json/wc/store',
    getProducts(){
        return axios.get(`${this.base}/products`)
    },
}